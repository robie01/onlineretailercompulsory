﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using OrderApi.Data;
using SharedModels;
using RestSharp;
using EasyNetQ;
using OrderApi.Infrastructure;

namespace OrderApi.Controllers
{
    [Route("api/Orders")]
    public class OrdersController : Controller
    {
        IRepository<Order> repository;
        IServiceGateway<Product> productServiceGateway;
        IMessagePublisher messagePublisher;

        public OrdersController(IRepository<Order> repos, IServiceGateway<Product> gateway, IMessagePublisher publisher)
        {
            repository = repos;
            productServiceGateway = gateway;
            messagePublisher = publisher;
        }

        // GET: api/orders
        [HttpGet]
        public IEnumerable<Order> Get()
        {
            return repository.GetAll();
        }

        // GET api/order/5
        [HttpGet("{id}", Name = "GetOrder")]
        public IActionResult Get(int id)
        {
            var item = repository.Get(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }



        // POST api/orders
        [HttpPost]
        public IActionResult Post([FromBody]Order order)
        {
            if (order == null)
            {
                return BadRequest();
            }
         
            foreach (var orderLine in order.OrderLines)
            {
               if (order.Id == orderLine.OrderId) { 

                        // Call product service to get the product ordered
                        var orderedProduct = productServiceGateway.Get(orderLine.ProductId);

                if (orderLine.Quantity <= orderedProduct.ItemsInStock)
                {
                    try
                    {
                        // Publish OrderStatusChangedMessage. If this operation
                        // fails, the order will not be created
                        messagePublisher.PublishOrderStatusChangedMessage(orderLine.ProductId,
                            orderLine.Quantity, "orderCompleted");

                        
                    }
                    catch
                    {
                        return StatusCode(500, "An error happened. Try again.");
                    }
                }
                else
                {
                    // If the order could not be created, "return no content".
                    return StatusCode(500, "Not enough items in stock.");
                }
            }

        }

            // Create order.
            order.Status = Order.OrderStatus.completed;
            var newOrder = repository.Add(order);
            return CreatedAtRoute("GetOrder", new { id = newOrder.Id }, newOrder);
        }

    }
}
