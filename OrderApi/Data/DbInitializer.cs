﻿using System.Collections.Generic;
using System.Linq;
using SharedModels;
using System;

namespace OrderApi.Data
{
    public class DbInitializer : IDbInitializer
    {
        // This method will create and seed the database.
        public void Initialize(OrderApiContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            // Look for any Products
            if (context.Orders.Any())
            {
                return;   // DB has been seeded
            }

            List<Product> prods = new List<Product> {
                new Product {Name = "Test1", Price = 20.9, ItemsInStock = 4, ItemsReserved = 2},
                new Product {Name = "Test2", Price = 2.9, ItemsInStock = 4, ItemsReserved = 1},
                new Product {Name = "Test3", Price = 21.9, ItemsInStock = 5, ItemsReserved = 3}
            };

            List<Order> orders = new List<Order>
            {
                new Order { Date = DateTime.Today, ProductId = 1, Quantity = 2, CustomerId = 1 },
                new Order { Date = DateTime.Today, ProductId = 2, Quantity = 3, CustomerId = 1 }
            };

            context.Orders.AddRange(orders);
            context.SaveChanges();
        }
    }
}
