﻿using SharedModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApiFinal.Data
{
    public class DbInitiazer : IDbInitializer
    {
        public void Initialize(CustomerApiFinalContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();


            // Look for any Products
            if (context.Customers.Any())
            {
                return;   // DB has been seeded
            }

            List<Customer> customers = new List<Customer>
            {
                new Customer {Name = "Tobie", Phone = 12458, Email = "test@dk.com", BillingAddress = "DK", CreditStanding = "approved" , ShippingAddress = "DK"  }
            };

            context.Customers.AddRange(customers);
            context.SaveChanges();
        }
    }
}
