﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApiFinal.Data
{
    public interface IDbInitializer
    {
        void Initialize(CustomerApiFinalContext context);
    }
}
