﻿using Microsoft.EntityFrameworkCore;
using SharedModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApiFinal.Data
{
    public class CustomerRepository : IRepository<Customer>
    {
        private readonly CustomerApiFinalContext db;
        public CustomerRepository(CustomerApiFinalContext context)
        {
            db = context;
        }
        Customer IRepository<Customer>.Add(Customer cust)
        {
            var newCustomer = db.Customers.Add(cust).Entity;
            db.SaveChanges();
            return newCustomer;
        }

        void IRepository<Customer>.Edit(Customer cust)
        {
            db.Entry(cust).State = EntityState.Modified;
            db.SaveChanges();
        }

        Customer IRepository<Customer>.Get(int id)
        {
            return db.Customers.FirstOrDefault(o => o.Id == id);
        }

        IEnumerable<Customer> IRepository<Customer>.GetAll()
        {
            return db.Customers.ToList();
        }

        void IRepository<Customer>.Remove(int id)
        {
            var customer = db.Customers.FirstOrDefault(p => p.Id == id);
            db.Customers.Remove(customer);
            db.SaveChanges();
        }
    }
}
