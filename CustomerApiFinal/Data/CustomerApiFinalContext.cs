﻿using Microsoft.EntityFrameworkCore;
using SharedModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApiFinal.Data
{
    public class CustomerApiFinalContext : DbContext
    {
        public CustomerApiFinalContext(DbContextOptions<CustomerApiFinalContext> dbContext) : base(dbContext)
        {

        }
        public DbSet<Customer> Customers { get; set; }
    }
}
