﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerApiFinal.Data;
using Microsoft.AspNetCore.Mvc;
using SharedModels;

namespace CustomerApiFinal.Controllers
{
        [Route("api/Customers")]
        public class CustomersController : Controller
        {
            private readonly IRepository<Customer> repository;

            public CustomersController(IRepository<Customer> repos)
            {
                repository = repos;
            }

            // GET: api/customers
            [HttpGet]
            public IEnumerable<Customer> Get()
            {
                return repository.GetAll();
            }

            // GET api/customers/5
            [HttpGet("{id}", Name = "GetCustomer")]
            public IActionResult Get(int id)
            {
                var item = repository.Get(id);
                if (item == null)
                {
                    return NotFound();
                }
                return new ObjectResult(item);
            }
        }
    }

